from utils import get_age, serialize, date_format
import datetime as dt
import json
import pytest

@pytest.mark.base
def test_upper():
    """Тестировка функции upper"""
    assert 'hello world'.upper() == 'HELLO WORLD'

@pytest.mark.base
def test_age_func():
    """Тестировка функции, возвращающей возраст по дню рождения"""
    now = dt.datetime.now()
    last_year_date = dt.datetime(now.year - 10, now.month, now.day) 
    assert 10 == get_age(last_year_date.strftime('%d.%m.%Y'))
    assert 0 == get_age(now.strftime('%d.%m.%Y'))

@pytest.mark.base
def test_serialization():
    """Тестировка функции преобразующей json в DataFrame"""
    json_data= json.dumps([
        { 'id': 1, 'name': 'Jhony', 'surname': 'Silverhand', 'city': 'Night-city' },
        { 'id': 2, 'name': 'Геральт', 'surname': None, 'city': 'Каэр Морхен' },
        { 'id': 3, 'name': 'Йеннифер', 'surname': None, 'city': 'Венгерберг' },
        { 'id': 4, 'name': 'Микаса', 'surname': 'Аккерман', 'city': 'Сигансина' },
    ])

    test_json = json.loads(json_data)
    test_df = serialize(test_json)
    assert test_df.shape[0] == len(test_json)

@pytest.mark.base
def test_date_format():
    """Тестировка функции преобразующую дату в нормальный вид"""
    assert date_format('1.1.2000') == '01.01.2000'
