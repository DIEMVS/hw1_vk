---
marp: true
theme: gaia
_class: lead
paginate: true
backgroundColor: #fff
backgroundImage: url('https://marp.app/assets/hero-background.jpg')
---
### Семинар наставника "Анализ данных в девелопменте" 
##### Работу выполнил: студент группы АДД 2023 Сатаев Д. А.


 
---

## Постановка задачи

1) Получение токена для авторизации в API VK.
2) Получение пользователей определенного города.
3) Обработка данных с помощью python.
4) Тестировка.
5) Визуализация данных в сервисе DataLens.
6) Верстка отчета в markdown.

[Ссылка на GitLab](https://gitlab.com/DIEMVS/hw1_vk)

[Ссылка на DataLens](https://datalens.yandex/h44k11wbi7gs6)

---

## Используемые тенхологии и сервисы

|  |  |
| ------ | ------ |
| Python | 3.11.4 |
| DataLens | - |
| GitLab CI/CD | - |
| Jupyter Notebook | - |
| Source Tree | - |

---

## Содержание проекта

| Файл | Описание |
| ------ | ------ |
| app.ipynb | Основной файл |
| tests.py | Файл с тестами |
| utils.py | Вспомогательные функции |
| pytest.ini | Конфигурация pytest |
| vk_api_client.py | Класс для доступа к API VK |
| .env.template | Шаблон для файла с перменными окружения |
| .gitlab-ci.yml | Конфигурация запуска CI |

---

## Логирование

- Логирование в файл
- Включение/выключение логирования из файла .env
- Уровень логирования из файла .env

![bg right:50% 95%](resources/logs.png)

---

## Содержание app.ipynb

- Получение собственных данных.
- Получение пользователей определенного города.
- Преобразование данных в датафрейм.
- Вывод топ мужских и женских имен.
- Сохранение датафрейма в CSV.

---

## Содержание utils.py

- serialize - функция для получения DataFrame из json.
- add_age_column - функция для добавления столбца с возрастом.
- date_format - функция для преобразования формата даты.
- drop_columns - функция для удаления столбцов.
- get_age - функция для получения возраста.

---

## Содержание tests.py

- test_upper - тест функции upper().
- test_age_func - тест функции возвращающей возраст по дню рождения.
- test_serialization - тест функции преобразующей json в DataFrame.
- test_date_format - тест функции преобразующую дату в нормальный вид.

---

## Содержание .gitlab-ci.yml

- stages - этапы тестировки.
- script - скрипт для запуска тестов.
- rules - правила для запуска (запуск только в ветке test).

---

## Repository graph

- мастер ветка main
- ветка develop
- ветка test
- ветки с tasks

[cсылка на GitLab](https://gitlab.com/DIEMVS/hw1_vk/-/tree/main?ref_type=heads)

![bg right:50% 95%](resources/repositiry_graph.png)

---
## CI

- запуск тестов в ветке test

[cсылка на GitLab](https://gitlab.com/DIEMVS/hw1_vk/-/jobs)

![bg right:50% 95%](resources/ci.png)

---

## Issue boards

- задачи
- сроки задач
- фактическое время затрат

[cсылка на GitLab](https://gitlab.com/DIEMVS/hw1_vk/-/boards/7193795)

![bg right:50% 65%](resources/issue_board.png)

---

# DataLens

- количество пользователей
- половозрастное распределение
- топ 10 мужских и женских имен

[Ссылка на DataLens](https://datalens.yandex/h44k11wbi7gs6)

![bg right:50% 95%](resources/datalens.jpg)