# HW1_VK


## Used services

| Service | Link |
| ------ | ------ |
| GitLab | https://gitlab.com/DIEMVS/hw1_vk |
| DataLens | https://datalens.yandex/h44k11wbi7gs6 |

## Features

- Import personal info from VK api.
- Import countries from VK api.
- Import cities from VK api.
- Search users in VK.

# Screenshots
![screenshot](report/resources/datalens.jpg)

## Enviroments
Put ".env" file in your root directory.
```sh
TOKEN=REPLACE_WITH_YOUT_TOKEN
API_VERSION=5.154
URL=https://api.vk.com/method
```

## Testing
Run all tests.
```sh
pytest tests.py 
```
Run tests marked as base
```sh
pytest tests.py -m base.
```
Run test marked as vk_api (Token required).
```sh
pytest tests.py -m vk_api 
```
