import pandas as pd
import datetime as dt

def serialize(json) -> pd.DataFrame:
    """Сериализация json в DataFrame"""
    return pd.DataFrame(json)
    
def add_age_column(df) -> pd.DataFrame:
    """Добавить колонку с возрастом."""
    df['bdate'] = df['bdate'].astype(str)
    df = df[df['bdate'] != 'nan']
    df = df[df['bdate'].str.match('^\d+.\d+.\d{4}$')]
    df['bdate'] = df['bdate'].apply(lambda b: date_format(b))
    df['age'] = df['bdate'].apply(lambda b: get_age(b))

    return df

def date_format(str: str) -> str:
   return '.'.join([f'0{s}' if len(s) == 1 else s for s in str.split('.')])
        

def drop_columns(df, columns) -> pd.DataFrame:
    for c in columns:
        df = df.drop(c, axis = 1)
    return df

def get_datetime_from_str(str) -> dt.datetime:
    return dt.datetime.strptime(str, '%d.%m.%Y')

def get_age(bdate) -> int:
    bdate_dt = get_datetime_from_str(bdate)
    now = dt.datetime.now()
    return now.year - bdate_dt.year - ((now.month, now.day) < (bdate_dt.month, bdate_dt.day))