import os
from dotenv import load_dotenv
import requests
import json
import logging

class VkApiClient:
    # Токен аутентификации
    __token = None
    # Версия
    __version = None
    # URL
    __url = None
    # логирование
    __log_enable = False
    # уровень логирования
    __log_level = None

    __logger = None

    def __init__(self):
        load_dotenv()
        self.__token = os.environ.get('TOKEN')
        self.__version = os.environ.get('API_VERSION')
        self.__url = os.environ.get('URL')
        self.__log_enable = os.environ.get('LOG_ENABLE')
        self.__log_level = os.environ.get('LOG_LEVEL')

        self.__logger = logging.getLogger(__name__)
        self.__logger.setLevel(self.__log_level)

        if(self.__log_enable == False):
            self.__logger.setLevel(0)

        handler = logging.FileHandler(f"{__name__}.log", mode='w')
        formatter = logging.Formatter("%(name)s %(asctime)s %(levelname)s %(message)s")

        handler.setFormatter(formatter)
        self.__logger.addHandler(handler)

        self.__logger.debug('init')


    def get_user_profile_info(self) -> json:
        """Возвращает профиль пользователя."""
        self.__logger.debug("get_user_profile_info")
        return self.base_request(path = 'account.getProfileInfo')
    
    def get_countries(self, code: list = ['RU']) -> json:
        """Возвращает список стран.
        param: `code` Перечисленные через запятую двухбуквенные коды стран в стандарте ISO 3166-1 alpha-2, 
        для которых необходимо выдать информацию.
        """
        self.__logger.debug("get_countries")
        data = {
            'code': code
        }
        return self.base_request(path = 'database.getCountries', params = data)
    
    def get_cities(self, q: str, country_id: int) -> json:
        """Возвращает список городов.
        param: `q` Строка поискового запроса. Например, «Санкт». Максимальная длина строки — 15 символов.
        param: `country_id` Идентификатор страны, полученный database.getCountries.
        """
        self.__logger.debug("get_cities")
        data = {
            'country_id': country_id,
            'q': q
        }
        return self.base_request(path = 'database.getCities', params = data)
    
    def search_users(
            self, 
            city_id: int,
            sex: int = 0, 
            q: str = '',
            count: int = 1000, 
            fields = str, 
            sort: int = 0,
            age_to = 100):
        """Возвращает список пользователей.
        param: `q` Строка поискового запроса. Например, Вася Бабич.
        param: `sort` Сортировка результатов. Возможные значения: 0 - дата регистрации, 0 - по популярности.
        param: `offset` Смещение относительно первого найденного пользователя для выборки определенного подмножества.
        param: `count` Количество возвращаемых пользователей.
        param: `fields` Список дополнительных полей профилей, которые необходимо вернуть.
        param: `city_id` Идентификатор города для обратной совместимости.
        param: `sex` Пол. 1 - женщина, 2 - мужчина, 0 - любой.
        """
        self.__logger.debug("search_users")
        data = {
            'q': q,
            'sort': sort,
            'sex': sex,
            'count': count,
            'fields': fields,
            'city_id': city_id,
            'age_to': age_to
        }

        return self.base_request(path = 'users.search', params = data)



    def base_request(self, path: str, params: dict[str, any] = None) -> json:
        """Запрос.
        param: `path` Путь запроса.
        param: `params` Параметры запроса.
        """
        self.__logger.info("base_request")
        assert self.__url is not None

        required_params = { 
            'access_token': self.__token,
            'v': self.__version
        }

        if(params is not None):
            required_params = { **required_params, **params }

        try:
            res = requests.post(
            url=f'{self.__url}/{path}',
            data=required_params
            )

            self.__logger.debug(res.text)

            json_res = json.loads(res.text)

            assert res.ok == True

            return json_res['response']
        except requests.exceptions.RequestException as e: 
            self.__logger.debug(e)
            raise SystemExit(e)

        
    



    